# Umamusume Soundtrack Ripper
A python script for extracting the soundtracks from the game *Umamusume Pretty Derby*.

## Dependencies
- `python3` (Obviously)
- `tqdm` (For python)
- `sqlite3` (For reading game files)
- `vgmstream-cli` (For conversion)
- `flac` (For conversion)

## Usage
```shell
umamusume-soundtrack-ripper GAME_DIR OUTPUT_DIR
```
Where `GAME_DIR` is the DMM game installation, usually located in `C:\Users\your_user_name\AppData\LocalLow\Cygames\umamusume`.
