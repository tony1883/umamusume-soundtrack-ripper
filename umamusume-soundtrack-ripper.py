#!/usr/bin/env python3
import sys
import os
import subprocess
import multiprocessing
from pathlib import Path
from tqdm import tqdm


game_asset_dir = sys.argv[1]
output_dir = sys.argv[2]
temp_dir = 'temp'
mapping_file_path = Path("mapping.csv")

def restore_file(token, path):
	file_grp = token[:2]
	src_file_path = game_asset_dir_path.joinpath('dat',file_grp, token)
	dest_file_path = temp_dir_path.joinpath(Path(path).name)
	if not dest_file_path.parent.exists():
		dest_file_path.parent.mkdir(parents=True, exist_ok=True)
	if not src_file_path.exists():
		tqdm.write("ERROR: " + path + " not found at indexed location, check if the index provided is up to date or game asset is complete")
		return
	copyfile(path, src_file_path, dest_file_path)

def copyfile(name, src, dest , bufsize=524288):
	length=os.path.getsize(src)
	pbar = tqdm(total=length, unit="byte", unit_scale=True, leave=False)
	pbar.set_description("Copying " + name)
	with open(src, 'rb') as f1:
		with open(dest, 'wb') as f2:
			while length:
				chunk = min(bufsize, length)
				data = f1.read(chunk)
				f2.write(data)
				length -= chunk
				pbar.update(chunk)
	pbar.close()

def extract_mapping_from_db():
	db_file_path = game_asset_dir_path.joinpath('meta')
	print("Reading file mapping from game data: " + str(db_file_path))
	with open(mapping_file_path, "w") as outfile:
		subprocess.run(['sqlite3', '-csv', str(db_file_path), "SELECT h, n FROM a WHERE m = 'sound' AND n LIKE 'sound/b/%';"], stdout=outfile)

def convert_cri_file(path):
	sub_stream = 1
	while sub_stream >= 1:
		s = subprocess.run(['vgmstream-cli', '-s', str(sub_stream), '-o', output_dir_path.joinpath(path.stem+"_?s.wav"), path], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
		if s.returncode > 0:
			sub_stream = 0
		else:
			sub_stream += 1

def convert_cri_files():
	print("Extracting audio from Atom Wave Bank files")
	dir = temp_dir_path.joinpath()
	files = list(dir.rglob('*.awb'))
	p = multiprocessing.Pool(processes = max(1, int(multiprocessing.cpu_count()/2)))
	list(tqdm(p.imap_unordered(convert_cri_file, files), total=len(files)))
	p.close()
	p.join()

def convert_wav_file(f):
	s = subprocess.run(['flac', f, "-o", output_dir_path.joinpath(f.stem+".flac"), '-f'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
	if s.returncode == 0:
		f.unlink()

def convert_wav_files():
	print("Compressing extracted audio")
	files = list(output_dir_path.rglob('*.wav'))
	p = multiprocessing.Pool(processes = multiprocessing.cpu_count()-1)
	list(tqdm(p.imap_unordered(convert_wav_file, files), total=len(files)))
	p.close()
	p.join()

def remove_dir(pth: Path):
	for child in pth.iterdir():
		if child.is_file():
			child.unlink()
		else:
			remove_dir(child)
	pth.rmdir()


game_asset_dir_path = Path(game_asset_dir)
output_dir_path = Path(output_dir)
output_dir_path.mkdir(parents=True, exist_ok=True)
temp_dir_path = Path(temp_dir)
temp_dir_path.mkdir(parents=True, exist_ok=True)
extract_mapping_from_db()
with open(mapping_file_path) as mapping:
	pbar = tqdm(total=sum(1 for line in mapping), leave=True)
	mapping.seek(0)
	mapping_entry = mapping.readline()
	while mapping_entry:
		entry = mapping_entry.split(',')
		restore_file(entry[0], entry[1].strip())
		pbar.update()
		mapping_entry = mapping.readline()
	pbar.close()
convert_cri_files()
convert_wav_files()
remove_dir(temp_dir_path)